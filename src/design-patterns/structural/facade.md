Facade
--------

Facade is a structural design pattern that provides a simplified interface to a library, a framework, or any other complex set of classes.

- A facade is a class that provides a simple interface to a complex subsystem which contains lots of moving parts

- While Facade decreases the overall complexity of the application, it also helps to move unwanted dependencies to one place

### Usage

- Use the Facade pattern when you need to have a limited but straightforward interface to a complex subsystem.

- Use the Facade when you want to structure a subsystem into layers.

----
Typescript 

It’s especially handy when working with complex libraries and APIs.