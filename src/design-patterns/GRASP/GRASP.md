GRASP(general responsibility assignment software patterns)
======
GRASP defines nine basic OO design principles or basic building blocks in design.

GRASP for basic patterns of assigning responsibilities, and GoF for more advanced design ideas

- Responsibility can be:
  - accomplished by a single object.
  - or a group of object collaboratively accomplish a responsibility.
- GRASP helps us in deciding which responsibility should be
assigned to which object/class.
- Identify the objects and responsibilities from the problem domain,
and also identify how objects interact with each other.
- Define blue print for those objects – i.e. class with methods
implementing those responsibilities. 

