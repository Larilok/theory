MAGIC
======
Number
- Including unexplained numbers in algorithms

String
- Implementing presumably unlikely input scenarios, such as comparisons with very specific strings, to mask functionality