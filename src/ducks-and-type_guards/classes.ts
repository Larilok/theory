export class A {
  x: number
  constructor(x: number) {
    this.x = x
  }
}
export class B {
  x: number
  y: string
  constructor(x: number, y: string) {
    this.x = x
    this.y = y
  }
}
