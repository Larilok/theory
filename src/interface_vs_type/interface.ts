interface IAnimal {
  name: string
}

interface IBear extends IAnimal {
  honey: boolean
}

// --------------------------------------

interface IWindow {
  title: string
}

interface IWindow {
  ts: typeof import('typescript')
}
