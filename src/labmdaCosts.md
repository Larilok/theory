Lambda service
=======

### AWS Lambda 

Free Tier
- 400,000 seconds of compute time per month for AWS Lambda
- 1,000,000 free requests per month for AWS Lambda

Pricing
- Requests	$0.20 per 1M requests
- Duration	$0.0000166667 for every GB-second
- Memory
  - 128MB $0.0000002083
  - 512	$0.0000008333
  - 1024	$0.0000016667
  - 1536	$0.0000025000
  - 2048	$0.0000033333
  - 3008	$0.0000048958
- Provisioned Concurrency	$0.0000050147 for every GB-second


RDS
- 750 hours of Amazon RDS Single-AZ db.t2.micro Instances