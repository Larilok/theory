YAGNI(You aren’t going to need it)
====
It's a statement that some capability we presume our software needs in the future should not be built now because "you aren't gonna need it"

Yagni only applies to capabilities built into the software to support a presumptive feature, it does not apply to effort to make the software easier to modify. Yagni is only a viable strategy if the code is easy to change, so expending effort on refactoring isn't a violation of yagni because refactoring makes the code more malleable