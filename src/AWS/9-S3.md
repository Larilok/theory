S3
========
Amazon S3 provides virtually limitless storage on the internet.

Amazon S3 stores data as objects within buckets. An object is a file and any optional metadata that describes the file. 

## Usage
- Backup and storage – Provide data backup and storage services for others.

- Application hosting – Provide services that deploy, install, and manage web applications.

- Media hosting – Build a redundant, scalable, and highly available infrastructure that hosts video, photo, or music uploads and downloads.

- Software delivery – Host your software applications that customers can download.

## Types

### S3 Intelligent-Tiering 
S3 Intelligent-Tiering works by storing objects in four access tiers: two low latency access tiers optimized for frequent and infrequent access, and two opt-in archive access tiers designed for asynchronous access that are optimized for rare access. Objects uploaded or transitioned to S3 Intelligent-Tiering are automatically stored in the Frequent Access tier. S3 Intelligent-Tiering works by monitoring access patterns and then moving the objects that have not been accessed in 30 consecutive days to the Infrequent Access tier. Once you have activated one or both of the archive access tiers, S3 Intelligent-Tiering will automatically move objects that haven’t been accessed for 90 consecutive days to the Archive Access tier and then after 180 consecutive days of no access to the Deep Archive Access tier. If the objects are accessed later, S3 Intelligent-Tiering moves the objects back to the Frequent Access tier. 

### S3 Standard-IA
This combination of low cost and high performance make S3 Standard-IA ideal for long-term storage, backups, and as a data store for disaster recovery files.

### S3 Glacier
S3 Glacier provides three retrieval options that range from a few minutes to hours.

### S3 Glacier Deep Archive
Retain data sets for 7-10 years or longer. All objects stored in S3 Glacier Deep Archive are replicated and stored across at least three geographically-dispersed Availability Zones, protected by 99.999999999% of durability, and can be restored within 12 hours

## Pricing

### Free tier
New AWS customers receive 5GB of Amazon S3 storage in the S3 Standard storage class; 20,000 GET Requests; 2,000 PUT, COPY, POST, or LIST Requests; and 15GB of Data Transfer Out each month for one year.
