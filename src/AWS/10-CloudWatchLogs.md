Cloud Watch insights
==========

## Pricing

### Free Tier

- Metrics
  - Basic Monitoring Metrics (at 5-minute frequency)

  - 10 Detailed Monitoring Metrics (at 1-minute frequency)

  - 1 Million API requests (not applicable to GetMetricData and GetMetricWidgetImage)
- Dashboard
  - 3 Dashboards for up to 50 metrics per month
- Alarms
  - 10 Alarm metrics (not applicable to high-resolution alarms)  
- Logs
  - 5GB Data (ingestion, archive storage, and data scanned by Logs Insights queries)

### Paid tier

- Metrics
  - Amazon EC2 Detailed Monitoring pricing is based on the number of custom metrics, with no API charge for sending metrics.
- Dashboard
  - $3.00 per dashboard per month
- Alarms
  - Standard Resolution (60 sec)	$0.10 per alarm metric  
  - High Resolution (10 sec)	$0.30 per alarm metric
  - Standard Resolution Anomaly Detection	$0.30 per alarm
  - High Resolution Anomaly Detection	$0.90 per alarm
  - Composite	$0.50 per alarm
- Logs. All log types. There is no Data Transfer IN charge for any of CloudWatch.  
  - Collect (Data Ingestion)	$0.63 per GB
  - Store (Archival)	$0.0324 per GB
  - Analyze (Logs Insights queries)	$0.0063 per GB of data scanned


