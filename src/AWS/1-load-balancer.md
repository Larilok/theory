Load balancer
==========

- Round Robin

Requests are distributed across the group of servers sequentially.
- Weighted RR (CPU, RAM)

- Least connections

- Weighted Least Connections

- The Least Response Time

The application server that is responding the fastest receives the next request.

--------------------
Application Load balancer

The default routing algorithm is round robin; alternatively, you can specify the least outstanding requests routing algorithm.
