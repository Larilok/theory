CloudWatch
===============
Amazon CloudWatch monitors your Amazon Web Services (AWS) resources and the applications you run on AWS in real time.

- Displays metrics about every AWS service you use.
- You can create alarms that watch metrics and send notifications or automatically make changes to the resources you are monitoring when a threshold is breached.
- CloudWatch Events are deprecated. Use Amazon Event Bridge 