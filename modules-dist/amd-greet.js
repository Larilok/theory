define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.greet = void 0;
    function greet() {
        return 'Hello world';
    }
    exports.greet = greet;
});
